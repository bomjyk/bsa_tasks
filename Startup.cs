using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProjectStructure.DAL.Context;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Extensions;

namespace ProjectStructure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "ProjectStructure", Version = "v1"});
            });
            services.AddDbContext<ProjectContext>(options =>
                options.UseInMemoryDatabase("test"));
                //options.UseSqlServer(Configuration.GetConnectionString("ProjectDatabase")));
            
            services.AddMapper();
            services.AddRepositories();
            services.AddUnitOfWork();
            services.AddServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure v1"));
            }

            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            
        }
        public static void DatabaseInitializator(ProjectContext context)
        {
                    context.Teams.AddRange(
                        new[]
                        {
                            new Team()
                            {
                                Id = 1,
                                Name = "McDonalds",
                                CreatedAt = DateTime.Now
                            },
                            new Team()
                            {
                                Id = 2,
                                Name = "Carnegie",
                                CreatedAt = DateTime.Now
                            },
                            new Team()
                            {
                                Id = 3,
                                Name = "LosAngelesLakers",
                                CreatedAt = DateTime.Now
                            }
                        });
                    context.SaveChanges();
            context.Users.AddRange(
                    new User()
                    {
                        Id = 1,
                        FirstName = "Oleg",
                        LastName = "Myhailuk",
                        Email = "oleg.myhailuk@gmail.com",
                        BirthDay = new DateTime(1990,06,28),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                    },
                    new User()
                    {
                        Id = 2,
                        FirstName = "Misha",
                        LastName = "Myhailuk",
                        Email = "misha.myhailuk@gmail.com",
                        BirthDay = new DateTime(1999,01,11),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                    },
                    new User()
                    {
                        Id = 3,
                        FirstName = "Olena",
                        LastName = "Chorna",
                        Email = "olena.chorna@gmail.com",
                        BirthDay = new DateTime(1970,10,20),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                    },
                    new User()
                    {
                        Id = 4,
                        FirstName = "Viktor",
                        LastName = "Bur",
                        Email = "viktor.bur@gmail.com",
                        BirthDay = new DateTime(1960,11,02),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                    },
                    new User()
                    {
                        Id = 5,
                        FirstName = "Yaroslav",
                        LastName = "Vitok",
                        Email = "yaroslav.vitok@gmail.com",
                        BirthDay = new DateTime(2001,01,14),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 3)
                    }    
                    );
            context.SaveChanges();
                    context.Projects.AddRange(
                            new Project() {
                                Id = 1,
                                Author = context.Users.FirstOrDefault(u => u.Id == 1),
                                Team = context.Teams.FirstOrDefault(t => t.Id == 1),
                                Name = "BestProject",
                                Description = "Best ever made project",
                                Deadline = new DateTime(2030,10,10),
                                CreatedAt = new DateTime(2021,06,30)
                            });
                    context.SaveChanges();
                    context.Tasks.AddRange(
            new Task()
                            {
                                Id = 1,
                                Description  = "Clean kot",
                                Name = "Clean a Cat",
                                Performer = context.Users.FirstOrDefault(u => u.Id == 1),
                                Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                                State = (TaskState)3,
                                CreatedAt = new DateTime(2021,06,30),
                                FinishedAt = new DateTime(2020,10,20),
                            },
                            new Task()
                            {
                                Id = 2,
                                Description  = "Clean Pes",
                                Name = "Clean a Dog",
                                Performer = context.Users.FirstOrDefault(u => u.Id == 2),
                                Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                                State = (TaskState)3,
                                CreatedAt = new DateTime(2021,06,30),
                                FinishedAt = new DateTime(2021,10,20),
                            }
            
                    );
                context.SaveChanges();
        }
    }
}