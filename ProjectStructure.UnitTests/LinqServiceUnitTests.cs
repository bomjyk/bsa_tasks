﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using FakeItEasy;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.UnitTests
{
    public class LinqServiceUnitTests
    {
        [Fact]
        public void GetTaskNumberByUserId_WhenIdIsValid_GetCorrectNumberOfTasks()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
                new User() {Id = 2, Email = "Oleg@gmail.com"},
                new User() {Id =3, Email =  "Mark@gmail.com"}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 2;
            //Act
            var result = service.GetTaskNumberByUserId(userId);
            //Assert
            Assert.Equal(0, result[projects.Find(p=> p.Id == 1)] );
            Assert.Equal(2, result[projects.Find(p=> p.Id == 2)]);
            Assert.Equal(0, result[projects.Find(p => p.Id == 3)]);
        }
        [Fact]
        public void GetTaskNumberByUserId_WhenIdIsOutOfPossibleRange_ThenDictionaryHasOnly0()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
                new User() {Id = 2, Email = "Oleg@gmail.com"},
                new User() {Id =3, Email =  "Mark@gmail.com"}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 4;
            //Act
            var result = service.GetTaskNumberByUserId(userId);
            //Assert
            Assert.Equal(0, result[projects.Find(p=> p.Id == 1)] );
            Assert.Equal(0, result[projects.Find(p=> p.Id == 2)]);
            Assert.Equal(0, result[projects.Find(p => p.Id == 3)]);
        }
        [Fact]
        public void GetTaskListForUser_WhenUserIdIsInRange_ThenGetNormalList()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
                new User() {Id = 2, Email = "Oleg@gmail.com"},
                new User() {Id =3, Email =  "Mark@gmail.com"}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Name ="Get mountain", Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Name ="Get mountain", Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Name ="Get mountain", Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Name ="Get mountain", Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Name ="Get mountain", Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Name ="Get mountain", Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 1;
            //Act
            var result = service.GetTasksListForUser(userId);
            //Assert
            Assert.Equal(2, result.Count);
        }
        [Fact]
        public void GetTaskListForUser_WhenTasksNameIsNullOrLargerThan_ThenGet0Tasks()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
                new User() {Id = 2, Email = "Oleg@gmail.com"},
                new User() {Id =3, Email =  "Mark@gmail.com"}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Name = null, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Name = new string('=', 60), Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Name ="Get mountain", Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Name ="Get mountain", Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Name ="Get mountain", Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Name ="Get mountain", Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 1;
            //Act
            var result = service.GetTasksListForUser(userId);
            //Assert
            Assert.Equal(0, result.Count);
        }
        [Fact]
        public void GetListOfFinishedTasksInCurrentYearForUser_WhenAllTasksFinishedInCurrentYear_ThenGetListWithAllTasks()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, 
                    Name = "Get mountain", 
                    Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = DateTime.Now
                },
                new Task() { Id = 2, 
                    Name = "Get mountain", 
                    Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = DateTime.Now
                },
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 1;
            //Act
            var result = service.GetListOfFinishedTasksInCurrentYearForUser(userId);
            //Assert
            Assert.Equal(2, result.Count);
        }
        [Fact]
        public void GetListOfFinishedTasksInCurrentYearForUser_WhenNoOneTaskMadeInCurrentYear_ThenGetEmptyList()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com"},
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, 
                    Name = "Get mountain", 
                    Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = null
                },
                new Task() { Id = 2, 
                    Name = "Get mountain", 
                    Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = DateTime.Now.AddYears(-2)
                },
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            int userId = 1;
            //Act
            var result = service.GetListOfFinishedTasksInCurrentYearForUser(userId);
            //Assert
            Assert.Equal(0, result.Count);
        }
        [Fact]
        public void GetListOfTeamsSortedByRegistration_WhenAllUsersAreOlderThenAgeLimit_ThenGetFullList()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
                new Team() {Id = 1, Name = "McDonalds", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Roshen", CreatedAt = DateTime.Now}
            };
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)
                },
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 2)}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            //Act
            var result = service.GetListOfTeamsSortedByRegistration();
            //Assert
            Assert.Equal(2, result.Count);
        }
        [Fact]
        public void GetListOfTeamsSortedByRegistration_WhenNoOneAreOlderThenAgeLimit_ThenGetEmptyList()
        {
            //Arrange   
            List<User> users = new List<User>()
            {
                new User() {Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-8), RegisteredAt = DateTime.Now},
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-6), RegisteredAt = DateTime.Now},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-4), RegisteredAt = DateTime.Now}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            //Act
            var result = service.GetListOfTeamsSortedByRegistration();
            //Assert
            Assert.Equal(0, result.Count);
        }
        [Fact]
        public void GetUsersByAlphabetWithSortedTasks_WhenAllUsersAndTasksAreCorrect_GetListOrderebByUsersAndSortedByTasks()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
                new Team() {Id = 1, Name = "McDonalds", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Roshen", CreatedAt = DateTime.Now}
            };
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)
                },
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 2)}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            //Act
            var result = service.GetUsersByAlphabetWithSortedTasks();
            var orderedByUserResult = result.OrderBy(r => r.User.FirstName).ToList();
            //Assert
            Assert.Equal(orderedByUserResult, result);
            
        }
        [Fact]
        public void GetUsersByAlphabetWithSortedTasks_WhenTasksWithoutUser_ThenThrownException()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
                new Team() {Id = 1, Name = "McDonalds", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Roshen", CreatedAt = DateTime.Now}
            };
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)
                },
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 2)}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1)},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2)},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, 
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, 
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 3, 
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, 
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, 
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, 
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            //Act
            //Assert
            Assert.Throws<NullReferenceException>(service.GetUsersByAlphabetWithSortedTasks);
            
        }
        [Fact]
        public void GetUserProjectAndTasks_WhenIdIsValid_ThenGetNormalList()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
                new Team() {Id = 1, Name = "McDonalds", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Roshen", CreatedAt = DateTime.Now}
            };
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)
                },
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 2)}
            };
            List<Project> projects = new List<Project>()
            {
                new Project() {Id = 1, Author = users.Find(u => u.Id == 1), CreatedAt = DateTime.Now},
                new Project() {Id = 2, Author = users.Find(u => u.Id == 2), CreatedAt = DateTime.Now},
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3), CreatedAt = DateTime.Now}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1)},
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = DateTime.Now
                },
                new Task() { Id = 3, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 4, Performer = users.Find(u => u.Id == 2),
                    Project = projects.Find(p => p.Id == 2)},
                new Task() { Id = 5, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)},
                new Task() { Id = 6, Performer = users.Find(u => u.Id == 3),
                    Project = projects.Find(p => p.Id == 3)}
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            var userId = 1;
            //Act
            var result = service.GetUserProjectAndTasks(userId);
            //Assert
            Assert.Equal(1, result.User.Id);
            Assert.Equal(projects.Find(p => p.Id == 1), result.LastProject);
            Assert.Equal(tasks.Find(t=>t.Id == 2), result.LongestTask);
            Assert.Equal(2, result.TasksNumber);
            Assert.Equal(1, result.NotFinishedTasks);
        }
        [Fact]
        public void GetProjectAndTaskStructure_WhenAllDataIsValid_ThenGetNormalStructure()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
                new Team() {Id = 1, Name = "McDonalds", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Roshen", CreatedAt = DateTime.Now}
            };
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = 1, Email = "Ivan@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)
                },
                new User() {Id = 2, Email = "Oleg@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 1)},
                new User() {Id =3, Email =  "Mark@gmail.com", BirthDay = DateTime.Now.AddYears(-12), RegisteredAt = DateTime.Now,
                    Team = teams.Find(t => t.Id == 2)}
            };
            List<Project> projects = new List<Project>()
            {
                new Project()
                {
                    Id = 1, Author = users.Find(u => u.Id == 1), CreatedAt = DateTime.Now,
                    Description = new string('-', 30)
                },
                new Project()
                {
                    Id = 2, Author = users.Find(u => u.Id == 2), CreatedAt = DateTime.Now,
                    Description = new string('-', 30)
                },
                new Project() {Id = 3, Author = users.Find(u => u.Id == 3), CreatedAt = DateTime.Now,
                    Description = new string('-', 30)}
            };
            List<Task> tasks = new List<Task>()
            {
                new Task() { Id = 1, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    Name = "McDonalds", Description = "McDonalds"
                },
                new Task() { Id = 2, Performer = users.Find(u => u.Id == 1),
                    Project = projects.Find(p => p.Id == 1),
                    FinishedAt = DateTime.Now,
                    Name = "Mc", Description = "Mc"
                },
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            var userId = 1;
            //Act
            var result = service.GetProjectAndTaskStructure();
            //Assert
            Assert.Equal(1, result.Project.Id);
            Assert.Equal(1, result.NumberOfPerformancers);
            Assert.Equal(tasks.Find(t=>t.Id == 1), result.LongestProjectTaskByDescription);
            Assert.Equal(tasks.Find(t => t.Id == 2), result.ShortestProjectTaskByName);
        }
        [Fact]
        public void GetProjectAndTaskStructure_WhenThereAreNoOneProjects_ThenThrowException()
        {
            //Arrange   
            List<Team> teams = new List<Team>()
            {
            };
            List<User> users = new List<User>()
            {
            };
            List<Project> projects = new List<Project>()
            {
            };
            List<Task> tasks = new List<Task>()
            {
            };
            IProjectRepository fakeProjectRepository = A.Fake<IProjectRepository>();
            A.CallTo(() => fakeProjectRepository.GetAll())
                .Returns(projects);
            ITaskRepository fakeTaskRepository = A.Fake<ITaskRepository>();
            A.CallTo(() => fakeTaskRepository.GetAll())
                .Returns(tasks);
            IUserRepository fakeUserRepository = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepository.GetAll())
                .Returns(users);
            LinqService service = new LinqService(
                taskRepository:fakeTaskRepository, 
                projectRepository:fakeProjectRepository,
                userRepository:fakeUserRepository);
            //Act
            //Assert
            Assert.Throws<InvalidOperationException>(service.GetProjectAndTaskStructure);
        }
    }
}