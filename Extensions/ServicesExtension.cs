using System;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Domain.DTO.Project;
using ProjectStructure.Domain.DTO.Task;
using ProjectStructure.Domain.DTO.Team;
using ProjectStructure.Domain.DTO.User;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Extensions
{
    public static class ServicesExtension
    {
        public static void AddRepositories(this IServiceCollection collection)
        {
            collection.AddScoped<IProjectRepository, ProjectRepository>();
            collection.AddScoped<ITaskRepository, TaskRepository>();
            collection.AddScoped<ITeamRepository, TeamRepository>();
            collection.AddScoped<IUserRepository, UserRepository>();
        }

        public static void AddServices(this IServiceCollection collection)
        {
            collection.AddScoped<IUserService, UserService>();
            collection.AddScoped<ITeamService, TeamService>();
            collection.AddScoped<ITaskService, TaskService>();
            collection.AddScoped<IProjectService, ProjectService>();
            collection.AddScoped<LinqService>();
        }

        public static void AddUnitOfWork(this IServiceCollection collection)
        {
            collection.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        public static void AddMapper(this IServiceCollection collection)
        {
            var map = MapperConfiguration().CreateMapper();
            collection.AddScoped(_ => map);
        }

        
        public static MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<TeamCreateDTO, Team>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForMember("CreatedAt", opt => opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<TeamUpdateDTO, Team>()
                    .ForMember("CreatedAt", opt => opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<Team, TeamReadDTO>();
                cnf.CreateMap<UserCreateDTO, User>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(u => u.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ReverseMap();
                cnf.CreateMap<UserUpdateDTO, User>()
                    .ForMember("RegisteredAt", opt => opt.Ignore())
                    .ForPath(u => u.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ReverseMap();
                cnf.CreateMap<User, UserReadDTO>()
                    .ForMember(u => u.TeamId, opt => opt.MapFrom(u => u.Team.Id));
                cnf.CreateMap<TaskCreateDTO, ProjectStructure.Domain.Entities.Task>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(t => t.Performer.Id, opt => opt.MapFrom(dto => dto.PerformerId) )
                    .ForPath(t => t.Project.Id, opt => opt.MapFrom(dto => dto.ProjectId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<TaskUpdateDTO, ProjectStructure.Domain.Entities.Task>()
                    .ForPath(t => t.Performer.Id, opt => opt.MapFrom(dto => dto.PerformerId) )
                    .ForPath(t => t.Project.Id, opt => opt.MapFrom(dto => dto.ProjectId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<ProjectStructure.Domain.Entities.Task, TaskReadDTO>()
                    .ForMember(t => t.PerformerId, opt => opt.MapFrom(t => t.Performer.Id))
                    .ForMember(t => t.ProjectId, opt => opt.MapFrom(t => t.Project.Id));
                cnf.CreateMap<ProjectCreateDTO, Project>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(t => t.Author.Id, opt => opt.MapFrom(dto => dto.AuthorId) )
                    .ForPath(t => t.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<ProjectUpdateDTO, Project>()
                    .ForPath(t => t.Author.Id, opt => opt.MapFrom(dto => dto.AuthorId) )
                    .ForPath(t => t.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<Project, ProjectReadDTO>()
                    .ForMember(p => p.AuthorId, opt => opt.MapFrom(p => p.Author.Id))
                    .ForMember(p => p.TeamId, opt => opt.MapFrom(p => p.Team.Id));
                cnf.CreateMap<ProjectReadDTO, Project>()
                    .ForPath(p => p.Author.Id, opt => opt.MapFrom(p => p.AuthorId))
                    .ForPath(p => p.Team.Id, opt => opt.MapFrom(p => p.TeamId))
                    .ReverseMap();
            });
            return config;
        }
    }
}