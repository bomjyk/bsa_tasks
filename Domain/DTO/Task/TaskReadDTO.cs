using System;

namespace ProjectStructure.Domain.DTO.Task
{
    public class TaskReadDTO
    {
        public int Id { get; set; }
        public int PerformerId { get; set; }
        public int ProjectId { get; set; }
        #nullable enable
        public string? Name { get; set; }
        public string? Description { get; set; }
        #nullable disable
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}