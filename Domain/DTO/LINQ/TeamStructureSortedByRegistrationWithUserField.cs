using System.Collections.Generic;

namespace ProjectStructure.Domain.DTO.LINQ
{
    public class TeamStructureSortedByRegistrationWithUserField
    {
        public int Id;
        public string Name;
        public List<Entities.User> Users = new List<Entities.User>();
    }
}