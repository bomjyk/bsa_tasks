using System.Collections.Generic;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Domain.DTO.LINQ
{
    public class AlphabetUserWithTask
    {
        public Entities.User User;
        public List<Entities.Task> Tasks;
    }
}