using System;

namespace ProjectStructure.Domain.DTO.User
{
    public class UserUpdateDTO
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        #nullable enable
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        #nullable disable
        public DateTime BirthDay { get; set; }
    }
}