namespace ProjectStructure.Domain.DTO.Team
{
    public class TeamUpdateDTO
    {
        public int Id { get; set; }
        #nullable enable
        public string? Name { get; set; }
        #nullable disable
    }
}