using System;

namespace ProjectStructure.Domain.Entities
{
    public class User
    {
        public int Id { get; set; }
        #nullable enable
        public Team? Team { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        #nullable disable
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}