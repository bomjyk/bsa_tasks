using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Domain.DTO.User;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        Task RegisterUser(UserCreateDTO user);
        Task UpdateUser(UserUpdateDTO user);
        Task<UserReadDTO> GetUser(int id);
        Task<IEnumerable<UserReadDTO>> GetAllUsers();
        Task DeleteUser(int id);
    }
}