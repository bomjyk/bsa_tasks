using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Domain.DTO.Team;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        Task CreateTeam(TeamCreateDTO team);
        Task DeleteTeam(int id);
        Task UpdateTeam(TeamUpdateDTO team);
        Task<TeamReadDTO> GetTeam(int id);
        Task<IEnumerable<TeamReadDTO>> GetAllTeams();
    }
}