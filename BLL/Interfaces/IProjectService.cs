using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Domain.DTO.Project;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        Task CreateProject(ProjectCreateDTO item);
        Task UpdateProject(ProjectUpdateDTO item);
        Task DeleteProject(int id);
        Task<ProjectReadDTO> GetProject(int id);
        Task<IEnumerable<ProjectReadDTO>> GetAllProjects();
    }
}