using System.Collections.Generic;
using ProjectStructure.Domain.DTO.Task;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        System.Threading.Tasks.Task AddNewTask(TaskCreateDTO item);
        System.Threading.Tasks.Task UpdateTask(TaskUpdateDTO item);
        System.Threading.Tasks.Task<TaskReadDTO> GetTask(int id);
        System.Threading.Tasks.Task<IEnumerable<TaskReadDTO>> GetAllTasks();
        System.Threading.Tasks.Task DeleteTask(int id);
    }
}