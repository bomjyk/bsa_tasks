using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.User;
using ProjectStructure.Domain.Entities;
using Task = ProjectStructure.Domain.Entities.Task;


namespace ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskService _taskService;
        private readonly IProjectService _projectService;

        public UserService(IMapper mapper, 
            IUnitOfWork unitOfWork, 
            ITaskService taskService, 
            IProjectService projectService)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _taskService = taskService;
            _projectService = projectService;
        }
        
        public async System.Threading.Tasks.Task RegisterUser(UserCreateDTO user)
        {
            User registerUser = _mapper.Map<User>(user);
            registerUser.RegisteredAt = DateTime.Now;
            if(user.TeamId != null) {
                registerUser.Team = await _unitOfWork.Teams.Get(user.TeamId.Value);
            }
            else
            {
                registerUser.Team = null;
            }
            _unitOfWork.Users.Create(registerUser);
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task UpdateUser(UserUpdateDTO user)
        {
            await _unitOfWork.Users.Update(_mapper.Map<User>(user));
            await _unitOfWork.Save();
        }

        public async Task<UserReadDTO> GetUser(int id)
        {
            return _mapper.Map<User, UserReadDTO>(await _unitOfWork.Users.Get(id));
        }

        public async Task<IEnumerable<UserReadDTO>> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserReadDTO>>(await _unitOfWork.Users.GetAll());
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            User user = await _unitOfWork.Users.Get(id);
            if(user != null)
            {
                (await _unitOfWork.Tasks.GetAll()).Where(t => t.Performer.Id == user.Id).ToList().ForEach(t =>
                {
                    _taskService.DeleteTask(t.Id);
                });
                (await _unitOfWork.Projects.GetAll()).Where(p => p.Author.Id == user.Id).ToList().ForEach(p =>
                {
                    _projectService.DeleteProject(p.Id);
                });
                _unitOfWork.Users.Delete(user);
                await _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}