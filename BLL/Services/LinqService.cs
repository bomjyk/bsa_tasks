using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class LinqService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;

        public LinqService(ITaskRepository taskRepository, 
            IProjectRepository projectRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _userRepository = userRepository;
        }
        
        public async System.Threading.Tasks.Task<Dictionary<Project, int>> GetTaskNumberByUserId(int userId)
        {
            return (await System.Threading.Tasks.Task.WhenAll(
                        (await _projectRepository
                    .GetAll())
                    .ToList()
                    .Select(async p =>
                    (
                        p,
                        (await _taskRepository.GetAll())
                        .Where(t => t.Project.Id == p.Id)
                        .Count(t => t.Performer.Id == userId)
                    ))))
                    .ToDictionary(
                         p => ( p).p,
                         p => (p).Item2
                    );
        }
        public async System.Threading.Tasks.Task<List<ProjectStructure.Domain.Entities.Task>> GetTasksListForUser(int userId)
        {
            const int taskNameSize = 45;
            return (
                    (await _taskRepository.GetAll())
                .Where(t => t.Performer.Id == userId 
                            && t.Name != null 
                            && t.Name.Length < taskNameSize)
                    ).ToList();
        }
        public async System.Threading.Tasks.Task<List<FinishedTaskOfYearForUser>> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            int currentYear = DateTime.Now.Year;
            return (await _taskRepository.GetAll())
                .Where(t => t.Performer.Id == userId && t.FinishedAt != null && t.FinishedAt.Value.Year == currentYear)
                .Select(t => new FinishedTaskOfYearForUser()
                {
                    Id = t.Id, 
                    Name =  t.Name
                })
                .ToList();
        }
        public async System.Threading.Tasks.Task<List<TeamStructureSortedByRegistrationWithUserField>> GetListOfTeamsSortedByRegistration()
        {
            const int ageLimit = 10;
            return (await _userRepository
                .GetAll())
                .Where(u => DateTime.Now.Year - u.BirthDay.Year > ageLimit)
                .GroupBy(u => u.Team)
                .Select(u => new TeamStructureSortedByRegistrationWithUserField()
                    {
                        Id = u.Key.Id,
                        Name = u.Key.Name,
                        Users = u
                            .Where(t => t.Team == u.Key)
                            .Select(t => t)
                            .OrderBy(u => u.RegisteredAt)
                            .ToList()
                    })
                .ToList();
        }
        public async System.Threading.Tasks.Task<List<AlphabetUserWithTask>> GetUsersByAlphabetWithSortedTasks()
        {
            return (await _taskRepository.GetAll())
                .GroupBy(t => t.Performer)
                .OrderBy(t => t.Key.FirstName)
                .Select(g => new AlphabetUserWithTask
                {
                    Tasks = g.Select(t => t).OrderByDescending(t => t.Name).ToList(),
                    User = g.Key
                })
                .ToList();
        }

        public async System.Threading.Tasks.Task<UserProjectAndTasks> GetUserProjectAndTasks(int userId)
        {
            var projects = (await _projectRepository.GetAll()).ToList();
            var tasks = (await _taskRepository.GetAll()).ToList();
            return projects
                .ToList()
                .Select(p => p.Author)
                .Where(u => u.Id == userId)
                .Select(u => new UserProjectAndTasks()
                {
                    User = u,
                    LastProject = projects
                        .ToList()
                        .Where(p => p.Author.Id == userId)
                        .OrderBy(p => p.CreatedAt)
                        .Last(),
                    TasksNumber = tasks
                        .Count(t =>
                            projects
                                .ToList()
                                .Where(p => p.Author.Id == userId)
                                .OrderBy(p => p.CreatedAt)
                                .Last().Id == t.Project.Id),
                    NotFinishedTasks = tasks
                        .Count(t => t.Performer.Id == userId && t.FinishedAt == null),
                    LongestTask = tasks
                        .Where(t => t.Performer.Id == userId)
                        .Where(t => t.FinishedAt != null)
                        .OrderBy(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
                })
                .First();
        }

        public async System.Threading.Tasks.Task<ProjectAndTaskStructure> GetProjectAndTaskStructure()
        {
            return await (await _projectRepository.GetAll())
                .ToList()
                .Select(async p => new ProjectAndTaskStructure()
                {
                    Project = p,
                    LongestProjectTaskByDescription = (await _taskRepository.GetAll())
                        .ToList()
                        .OrderBy(t => t.Description).LastOrDefault(),
                    ShortestProjectTaskByName = (await _taskRepository.GetAll())
                        .ToList()
                        .OrderByDescending(t => t.Name).LastOrDefault(),
                    NumberOfPerformancers = (await  System.Threading.Tasks.Task.WhenAll((await _projectRepository.GetAll())
                        .Select(async pr =>
                        {
                            var e = (await _taskRepository.GetAll())
                                .ToList()
                                .Count(t => t.Project.Id == pr.Id);
                            return new {pr, e};
                        })
                        .ToList()))
                        .Where( pr => ( pr).pr.Id == p.Id && 
                                     ( pr).e < 3 ||
                                      p.Description.Length > 20).ToList()
                        ?.Count() ?? 0
                })
                .First();
        }
    }
}