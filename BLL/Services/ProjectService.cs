using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.Project;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITaskService _taskService;
        
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper, ITaskService taskService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _taskService = taskService;
        }
        
        public async System.Threading.Tasks.Task CreateProject(ProjectCreateDTO item)
        {
            Project project = _mapper.Map<Project>(item);
            project.CreatedAt = DateTime.Now;;
            project.Author = await _unitOfWork.Users.Get(item.AuthorId);
            project.Team = await _unitOfWork.Teams.Get(item.TeamId);
            if (project.Author == null || project.Team == null) throw new Exception();
            _unitOfWork.Projects.Create(project);
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task UpdateProject(ProjectUpdateDTO item)
        {
            await _unitOfWork.Projects.Update(_mapper.Map<Project>(item));
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task DeleteProject(int id)
        {
            Project project = await _unitOfWork.Projects.Get(id);
            if (project != null)
            {
                (await _unitOfWork.Tasks.GetAll()).Where(t => t.Project == project).ToList().ForEach(t =>
                {
                    _taskService.DeleteTask(t.Id);
                });
                _unitOfWork.Projects.Delete(project);
                await _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
        }

        public async System.Threading.Tasks.Task<ProjectReadDTO> GetProject(int id)
        {
            return _mapper.Map<ProjectReadDTO>(await _unitOfWork.Projects.Get(id));
        }

        public async System.Threading.Tasks.Task<IEnumerable<ProjectReadDTO>> GetAllProjects()
        {
            return _mapper.Map<IEnumerable<ProjectReadDTO>>(await _unitOfWork.Projects.GetAll());
        }
    }
}