using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.Team;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectService = projectService;
        }
        
        public async System.Threading.Tasks.Task CreateTeam(TeamCreateDTO team)
        {
            Team teamToCreate = _mapper.Map<Team>(team);
            teamToCreate.CreatedAt = DateTime.Now;
            _unitOfWork.Teams.Create(teamToCreate);
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            Team team = await _unitOfWork.Teams.Get(id);
            if(team != null)
            {
                (await _unitOfWork.Users.GetAll()).Where(u => u.Team == team).ToList().ForEach(u => u.Team = null);
                (await _unitOfWork.Projects.GetAll()).Where(p => p.Team == team).ToList().ForEach(p =>
                {
                    _projectService.DeleteProject(p.Id);
                });
                _unitOfWork.Teams.Delete(team);
                await _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
            
        }

        public async System.Threading.Tasks.Task UpdateTeam(TeamUpdateDTO team)
        {
            Team teamToUpdate = _mapper.Map<Team>(team);
            await _unitOfWork.Teams.Update(teamToUpdate);
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task<TeamReadDTO> GetTeam(int id)
        {
            return _mapper.Map<TeamReadDTO>(await _unitOfWork.Teams.Get(id));
        }

        public async System.Threading.Tasks.Task<IEnumerable<TeamReadDTO>> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamReadDTO>>(await _unitOfWork.Teams.GetAll());
        }
    }
}