using System;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.Task;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public async System.Threading.Tasks.Task AddNewTask(TaskCreateDTO item)
        {
            Task task = _mapper.Map<Task>(item);
            task.CreatedAt = DateTime.Now;
            task.Performer = await _unitOfWork.Users.Get(item.PerformerId);
            task.Project = await _unitOfWork.Projects.Get(item.ProjectId);
            if (task.Performer == null || task.Performer == null) throw new Exception();
            _unitOfWork.Tasks.Create(task);
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task UpdateTask(TaskUpdateDTO item)
        {
            await _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
            await _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task<TaskReadDTO> GetTask(int id)
        {
            return _mapper.Map<TaskReadDTO>(await _unitOfWork.Tasks.Get(id));
        }

        public async System.Threading.Tasks.Task<IEnumerable<TaskReadDTO>> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskReadDTO>>(await _unitOfWork.Tasks.GetAll());
        }

        public async System.Threading.Tasks.Task DeleteTask(int id)
        {
            Task task = await _unitOfWork.Tasks.Get(id);
            if (task != null)
            {
                _unitOfWork.Tasks.Delete(task);
                await _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
            
        }
    }
}