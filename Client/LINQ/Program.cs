﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LINQ.Services;

namespace LINQ
{
    class Program
    {
        static async Task Main(string[] args)
        {

            bool closeFlag = false;
            Console.WriteLine("Hello =)");
            string menuString = "Menu:\n";
            MenuService.menuWithParameter
                .Select( m => new {m.Key, m.Value.Method.Name})
                .ToList()
                .ForEach(m => menuString += m.Key + " " + m.Name + "\n");
            MenuService.menuWithoutParametr
                .Select(m => new {m.Key, m.Value.Method.Name})
                .ToList()
                .ForEach(m => menuString += m.Key + " " + m.Name + "\n");
            while (!closeFlag)
            {
                Console.Write(menuString);
                Console.WriteLine("Write number of action that you want to do (write only number)");
                string choice = Console.ReadLine();
                if (MenuService.menuWithParameter.Keys.Contains(choice))
                {
                    Console.WriteLine("Write id, that you want to get");
                    try
                    {
                        int id = Int32.Parse(Console.ReadLine());
                        Console.Write(await MenuService.menuWithParameter[choice].Invoke(id));
                    }
                    catch
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
                else if (MenuService.menuWithoutParametr.Keys.Contains(choice))
                {
                    try
                    {
                        Console.Write(await MenuService.menuWithoutParametr[choice].Invoke());
                    }
                    catch
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
                else
                {
                    Console.WriteLine("Sorry, there is no options with such letter");
                }
            }
        }
    }
}