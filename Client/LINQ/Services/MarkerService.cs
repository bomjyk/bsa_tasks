﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using ProjectStructure.Domain.Entities;
using Task = System.Threading.Tasks.Task;

namespace LINQ.Services
{
    public class MarkerService
    {
        private static readonly string _apiRoute = "https://localhost:5001/api/";
        private static readonly string _tasks = "Tasks";
        private static async Task<string> GetJsonByRoute(string route)
        {
            string json;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            using (HttpClient client = new HttpClient(clientHandler))
            {
                HttpResponseMessage tasksResponseMessage = await client.GetAsync(route);
                tasksResponseMessage.EnsureSuccessStatusCode();
                json = await tasksResponseMessage.Content.ReadAsStringAsync();
            }

            return json;
        }
        public async static Task<string> MarkRandomTaskWithDelay()
        {
            int delay = 1000;
            await Task.Delay(delay);
             var tasks = JsonConvert.DeserializeObject<IEnumerable<ProjectStructure.Domain.Entities.Task>>
                (await GetJsonByRoute(_apiRoute +  _tasks));
             Random random = new Random();
             if(tasks.Where(t => t.FinishedAt == null) != null)
             {
                 int choice = -1;
                 while(tasks.Where(t => t.Id == choice).FirstOrDefault() == null || 
                       tasks.Where(t => t.Id == choice).FirstOrDefault().FinishedAt != null ) {
                 choice = random.Next(tasks.Where(t => t.FinishedAt == null).First().Id,
                     tasks.Where(t => t.FinishedAt == null).Last().Id);
                 }
                 var task = tasks.FirstOrDefault(t => t.Id == choice);
                 task.FinishedAt = DateTime.Now;
                 
                 string json;
                 json = JsonConvert.SerializeObject(task);
                 HttpClientHandler clientHandler = new HttpClientHandler();
                 clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                 using (HttpClient client = new HttpClient(clientHandler))
                 {
                     HttpResponseMessage tasksResponseMessage = await client.PutAsync(_apiRoute +  _tasks,
                         new StringContent(json, Encoding.UTF8, "application/json"));
                     if (tasksResponseMessage.StatusCode == HttpStatusCode.OK)
                     {
                         return task.Id.ToString();
                     }
                     else
                     {
                         throw new Exception();
                     }
                 }
             }

             throw new Exception();
        }
    }
}