using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace LINQ.Services
{
    public class MainService 
    {
        private static readonly string _apiRoute = "https://localhost:5001/api/LINQ/";
        private static readonly string _taskNumberByUserId = "taskByUserId/id?id=";
        private static readonly string _tasksListForUser = "tasksForUser/id?id=";
        private static readonly string _listOfFinishedTasksInCurrentYearForUser = "listFinishedTasks/id?id=";
        private static readonly string _sortedListTeams = "sortedListTeams";
        private static readonly string _alphabetUsersWithSortedTasks = "alphabetUsersWithSortedTasks";
        private static readonly string _userProjectAndTasks = "getUserProjectAndTasks/id?id=";
        private static readonly string _projectAndTaskStructure = "getProjectAndTaskStructure";

        private static async Task<string> GetJsonByRoute(string route)
        {
            string json;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            using (HttpClient client = new HttpClient(clientHandler))
            {
                HttpResponseMessage tasksResponseMessage = await client.GetAsync(route);
                tasksResponseMessage.EnsureSuccessStatusCode();
                json = await tasksResponseMessage.Content.ReadAsStringAsync();
            }

            return json;
        }
        public static async Task<Dictionary<Project, int>> GetTaskNumberByUserId(int userId)
        {
            var data = JsonConvert.DeserializeObject<KeyValuePair<Project, int>[]>
                ((await GetJsonByRoute(_apiRoute + _taskNumberByUserId + userId)), new JsonSerializerSettings());
            if (data != null)
            {
                return data
                    .ToDictionary(kv => kv.Key, kv => kv.Value);
            }
            else
            {
                throw new Exception();
            }
        }

        public async static Task<List<ProjectStructure.Domain.Entities.Task>> GetTasksListForUser(int userId)
        {
            return JsonConvert.DeserializeObject<List<ProjectStructure.Domain.Entities.Task>>
                (await GetJsonByRoute(_apiRoute + _tasksListForUser + userId));
        }

        public async static Task<List<FinishedTaskOfYearForUser>> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            return JsonConvert.DeserializeObject<List<FinishedTaskOfYearForUser>>
                (await GetJsonByRoute(_apiRoute + _listOfFinishedTasksInCurrentYearForUser + userId));

        }
        public async static Task<List<TeamStructureSortedByRegistrationWithUserField>> GetListOfTeamsSortedByRegistration()
        {
            return JsonConvert.DeserializeObject<List<TeamStructureSortedByRegistrationWithUserField>>
                (await GetJsonByRoute(_apiRoute + _sortedListTeams ));
        }

        public async static Task<List<AlphabetUserWithTask>> GetUsersByAlphabetWithSortedTasks()
        {
            return JsonConvert.DeserializeObject<List<AlphabetUserWithTask>>
                (await GetJsonByRoute(_apiRoute + _alphabetUsersWithSortedTasks ));
        }
        public async static Task<UserProjectAndTasks> GetUserProjectAndTasks(int id)
        {
            return JsonConvert.DeserializeObject<UserProjectAndTasks>
                (await GetJsonByRoute(_apiRoute +  _userProjectAndTasks + id));
        }
        public async static Task<ProjectAndTaskStructure> GetProjectAndTaskStructure()
        {
            return JsonConvert.DeserializeObject<ProjectAndTaskStructure>
                (await GetJsonByRoute(_apiRoute +  _projectAndTaskStructure));
        }
    }
}