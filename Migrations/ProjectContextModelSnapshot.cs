﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.Migrations
{
    [DbContext(typeof(ProjectContext))]
    partial class ProjectContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProjectStructure.Domain.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2")
                        .HasColumnName("DeadlineAt");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(32)
                        .HasColumnType("nvarchar(32)");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            CreatedAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2030, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Best ever made project",
                            Name = "BestProject",
                            TeamId = 1
                        });
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int?>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("State")
                        .HasMaxLength(4)
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Clean kot",
                            FinishedAt = new DateTime(2020, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Clean a Cat",
                            PerformerId = 1,
                            ProjectId = 1,
                            State = 3
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Clean Pes",
                            FinishedAt = new DateTime(2021, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Clean a Dog",
                            PerformerId = 2,
                            ProjectId = 1,
                            State = 3
                        });
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasMaxLength(64)
                        .HasColumnType("nvarchar(64)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 6, 30, 14, 19, 59, 674, DateTimeKind.Local).AddTicks(6209),
                            Name = "McDonalds"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 30, 14, 19, 59, 678, DateTimeKind.Local).AddTicks(4316),
                            Name = "Carnegie"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 6, 30, 14, 19, 59, 678, DateTimeKind.Local).AddTicks(4359),
                            Name = "LosAngelesLakers"
                        });
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BirthDay = new DateTime(1990, 6, 28, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "oleg.myhailuk@gmail.com",
                            FirstName = "Oleg",
                            LastName = "Myhailuk",
                            RegisteredAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            BirthDay = new DateTime(1999, 1, 11, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "misha.myhailuk@gmail.com",
                            FirstName = "Misha",
                            LastName = "Myhailuk",
                            RegisteredAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 3,
                            BirthDay = new DateTime(1970, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "olena.chorna@gmail.com",
                            FirstName = "Olena",
                            LastName = "Chorna",
                            RegisteredAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            BirthDay = new DateTime(1960, 11, 2, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "viktor.bur@gmail.com",
                            FirstName = "Viktor",
                            LastName = "Bur",
                            RegisteredAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 5,
                            BirthDay = new DateTime(2001, 1, 14, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "yaroslav.vitok@gmail.com",
                            FirstName = "Yaroslav",
                            LastName = "Vitok",
                            RegisteredAt = new DateTime(2021, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 3
                        });
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.Project", b =>
                {
                    b.HasOne("ProjectStructure.Domain.Entities.User", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId");

                    b.HasOne("ProjectStructure.Domain.Entities.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId");

                    b.Navigation("Author");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.Task", b =>
                {
                    b.HasOne("ProjectStructure.Domain.Entities.User", "Performer")
                        .WithMany()
                        .HasForeignKey("PerformerId");

                    b.HasOne("ProjectStructure.Domain.Entities.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId");

                    b.Navigation("Performer");

                    b.Navigation("Project");
                });

            modelBuilder.Entity("ProjectStructure.Domain.Entities.User", b =>
                {
                    b.HasOne("ProjectStructure.Domain.Entities.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId");

                    b.Navigation("Team");
                });
#pragma warning restore 612, 618
        }
    }
}
