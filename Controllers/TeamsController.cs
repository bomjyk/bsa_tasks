using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Team;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet("id")]
        public async Task<ActionResult<TeamReadDTO>> GetTeamById(int id)
        {
            try
            {
                return Ok(await _teamService.GetTeam(id));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamReadDTO>>> GetAllTeams()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        [HttpPost]
        public async Task<ActionResult> AddTeam(TeamCreateDTO team)
        {
            await _teamService.CreateTeam(team);
            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            try
            {
                await _teamService.DeleteTeam(id);
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<ActionResult> UpdateTeam(TeamUpdateDTO team)
        {
            await _teamService.UpdateTeam(team);
            return Ok();
        }
    }
}