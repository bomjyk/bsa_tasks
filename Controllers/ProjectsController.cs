using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Project;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet("id")]
        public async System.Threading.Tasks.Task<ActionResult<ProjectReadDTO>> GetProjectById(int id)
        {
            try
            {
                ProjectReadDTO project = await _projectService.GetProject(id);
                return Ok(project);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetProjects()
        {
            return Ok(await _projectService.GetAllProjects());
        }

        [HttpPost]
        public async  System.Threading.Tasks.Task<ActionResult> AddProject([FromBody]ProjectCreateDTO project)
        {
            await _projectService.CreateProject(project);
            return Ok();
        }

        [HttpPut]
        public async System.Threading.Tasks.Task<ActionResult> UpdateProject(ProjectUpdateDTO project)
        {
            await _projectService.UpdateProject(project);
            return Ok();
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<ActionResult> DeleteProject(int id)
        {
            try
            {
                await _projectService.DeleteProject(id);
                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }
    }
}