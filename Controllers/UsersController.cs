using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.User;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpGet("id")]
        public async Task<ActionResult<UserReadDTO>> GetUser(int Id)
        {
            try
            {
                return Ok(await _userService.GetUser(Id));
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return Ok(await _userService.GetAllUsers());
        }

        [HttpPost]
        public async Task<ActionResult> AddUser(UserCreateDTO user)
        {
            try
            {
                await _userService.RegisterUser(user);
                return Ok();
            }
            catch
            {
                return new BadRequestResult();
            }
        }

        [HttpPut]
        public async Task<ActionResult> UpdateUser(UserUpdateDTO user)
        {
            await _userService.UpdateUser(user);
            return Ok();
        }
        
        [HttpDelete]
        public async Task<ActionResult> RemoveUser(int id)
        {
            try
            {
                await _userService.DeleteUser(id);
                return NoContent();
            }
            catch
            {
                return new BadRequestResult();
            }
        }
    }
}