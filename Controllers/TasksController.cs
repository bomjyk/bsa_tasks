using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Task;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("id")]
        public async System.Threading.Tasks.Task<ActionResult<TaskReadDTO>> GetTaskById(int id)
        {
            try
            {
                return Ok(await _taskService.GetTask(id));
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<TaskReadDTO>>> GetAllTasks()
        {
            return Ok(await _taskService.GetAllTasks());
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CreateTask(TaskCreateDTO task)
        {
            await _taskService.AddNewTask(task);
            return Ok();
        }

        [HttpPut]
        public async System.Threading.Tasks.Task<ActionResult> UpdateTask([FromBody]TaskUpdateDTO task)
        {
            await _taskService.UpdateTask(task);
            return Ok();
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<ActionResult> DeleteTask(int id)
        {
            try
            {
                await _taskService.DeleteTask(id);
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }
    }
}