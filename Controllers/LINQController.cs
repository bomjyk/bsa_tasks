using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;
        private readonly ProjectContext _context;

        public LinqController(LinqService linqService, ProjectContext context)
        {
            _linqService = linqService;
            _context = context;
        }

        [HttpGet("taskByUserId/id")]
        public async System.Threading.Tasks.Task<ActionResult<ICollection<(Project, int)>>> GetTaskByUserId(int id)
        {
            return  Ok(JsonConvert.SerializeObject((await _linqService.GetTaskNumberByUserId(id)).ToArray(), Formatting.Indented, new JsonSerializerSettings()));
        }
        
        [HttpGet("tasksForUser/id")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<Task>>> GetTasksListForUser(int id)
        {
            return  Ok(await _linqService.GetTasksListForUser(id));
        }
        
        [HttpGet("listFinishedTasks/id")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<FinishedTaskOfYearForUser>>> GetListOfFinishedTasksInCurrentYearForUser(int id)
        {
            return  Ok(JsonConvert.SerializeObject(await _linqService.GetListOfFinishedTasksInCurrentYearForUser(id), Formatting.Indented));
        }
        [HttpGet("sortedListTeams")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<TeamStructureSortedByRegistrationWithUserField>>> GetListOfTeamsSortedByRegistration()
        {
            return  Ok(JsonConvert.SerializeObject(await _linqService.GetListOfTeamsSortedByRegistration(), Formatting.Indented));
        }
        [HttpGet("alphabetUsersWithSortedTasks")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<AlphabetUserWithTask>>> GetUsersByAlphabetWithSortedTasks()
        {
            return  Ok(JsonConvert.SerializeObject(await _linqService.GetUsersByAlphabetWithSortedTasks(), Formatting.Indented));
        }
        [HttpGet("getUserProjectAndTasks/id")]
        public async System.Threading.Tasks.Task<ActionResult<UserProjectAndTasks>> GetUserProjectAndTasks(int id)
        {
            return  Ok(JsonConvert.SerializeObject(await _linqService.GetUserProjectAndTasks(id), Formatting.Indented));
        }
        [HttpGet("getProjectAndTaskStructure")]
        public async System.Threading.Tasks.Task<ActionResult<ProjectAndTaskStructure>> GetProjectAndTaskStructure()
        {
            return  Ok(JsonConvert.SerializeObject(await _linqService.GetProjectAndTaskStructure(), Formatting.Indented));
        }

        [HttpGet("seedDb")]
        public ActionResult SeedDb()
        {
            DatabaseInitializator(_context);
            return Ok();
        }
        public static void DatabaseInitializator(ProjectContext context)
        {
                    context.Teams.AddRange(
                        new[]
                        {
                            new Team()
                            {
                                Id = 1,
                                Name = "McDonalds",
                                CreatedAt = DateTime.Now
                            },
                            new Team()
                            {
                                Id = 2,
                                Name = "Carnegie",
                                CreatedAt = DateTime.Now
                            },
                            new Team()
                            {
                                Id = 3,
                                Name = "LosAngelesLakers",
                                CreatedAt = DateTime.Now
                            }
                        });
                    context.SaveChanges();
            context.Users.AddRange(
                    new User()
                    {
                        Id = 1,
                        FirstName = "Oleg",
                        LastName = "Myhailuk",
                        Email = "oleg.myhailuk@gmail.com",
                        BirthDay = new DateTime(1990,06,28),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                    },
                    new User()
                    {
                        Id = 2,
                        FirstName = "Misha",
                        LastName = "Myhailuk",
                        Email = "misha.myhailuk@gmail.com",
                        BirthDay = new DateTime(1999,01,11),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                    },
                    new User()
                    {
                        Id = 3,
                        FirstName = "Olena",
                        LastName = "Chorna",
                        Email = "olena.chorna@gmail.com",
                        BirthDay = new DateTime(1970,10,20),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                    },
                    new User()
                    {
                        Id = 4,
                        FirstName = "Viktor",
                        LastName = "Bur",
                        Email = "viktor.bur@gmail.com",
                        BirthDay = new DateTime(1960,11,02),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                    },
                    new User()
                    {
                        Id = 5,
                        FirstName = "Yaroslav",
                        LastName = "Vitok",
                        Email = "yaroslav.vitok@gmail.com",
                        BirthDay = new DateTime(2001,01,14),
                        RegisteredAt = new DateTime(2021,06,30),
                        Team = context.Teams.FirstOrDefault(t => t.Id == 3)
                    }    
                    );
            context.SaveChanges();
                    context.Projects.AddRange(
                            new Project() {
                                Id = 1,
                                Author = context.Users.FirstOrDefault(u => u.Id == 1),
                                Team = context.Teams.FirstOrDefault(t => t.Id == 1),
                                Name = "BestProject",
                                Description = "Best ever made project",
                                Deadline = new DateTime(2030,10,10),
                                CreatedAt = new DateTime(2021,06,30)
                            });
                    context.SaveChanges();
                    context.Tasks.AddRange(
            new Task()
                            {
                                Id = 1,
                                Description  = "Clean kot",
                                Name = "Clean a Cat",
                                Performer = context.Users.FirstOrDefault(u => u.Id == 1),
                                Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                                State = (TaskState)3,
                                CreatedAt = new DateTime(2021,06,30),
                                FinishedAt = new DateTime(2020,10,20),
                            },
                            new Task()
                            {
                                Id = 2,
                                Description  = "Clean Pes",
                                Name = "Clean a Dog",
                                Performer = context.Users.FirstOrDefault(u => u.Id == 2),
                                Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                                State = (TaskState)3,
                                CreatedAt = new DateTime(2021,06,30),
                                FinishedAt = new DateTime(2021,10,20),
                            }
            
                    );
                context.SaveChanges();
        }
    }
}