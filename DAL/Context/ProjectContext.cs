using System;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Context
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        {}
        public DbSet<Project> Projects { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseInMemoryDatabase(databaseName: "test");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ValidateDb(modelBuilder);
            base.OnModelCreating(modelBuilder);
            SeedDb(modelBuilder);
        }

        private void ValidateDb(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .Property(p => p.Name).HasMaxLength(32);
            modelBuilder.Entity<User>()
                .Property(u => u.RegisteredAt)
                .IsRequired();
            modelBuilder.Entity<Team>()
                .Property(t => t.Name)
                .HasMaxLength(64);
            modelBuilder.Entity<Task>()
                .Property(t => t.State)
                .HasMaxLength(4);
        }
        private void SeedDb(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasData(new[]
            {
                new Team()
                {
                    Id = 1,
                    Name = "McDonalds",
                    CreatedAt = DateTime.Now
                },
                new Team()
                {
                    Id = 2,
                    Name = "Carnegie",
                    CreatedAt = DateTime.Now
                },
                new Team()
                {
                    Id = 3,
                    Name = "LosAngelesLakers",
                    CreatedAt = DateTime.Now
                }
            });
            modelBuilder.Entity<User>().HasData(
                new 
                {
                    Id = 1,
                    FirstName = "Oleg",
                    LastName = "Myhailuk",
                    Email = "oleg.myhailuk@gmail.com",
                    BirthDay = new DateTime(1990,06,28),
                    RegisteredAt = new DateTime(2021,06,30),
                    TeamId = 1
                },
                new 
                {
                    Id = 2,
                    FirstName = "Misha",
                    LastName = "Myhailuk",
                    Email = "misha.myhailuk@gmail.com",
                    BirthDay = new DateTime(1999,01,11),
                    RegisteredAt = new DateTime(2021,06,30),
                    TeamId = 1
                },
                new 
                {
                    Id = 3,
                    FirstName = "Olena",
                    LastName = "Chorna",
                    Email = "olena.chorna@gmail.com",
                    BirthDay = new DateTime(1970,10,20),
                    RegisteredAt = new DateTime(2021,06,30),
                    TeamId = 2
                },
                new 
                {
                    Id = 4,
                    FirstName = "Viktor",
                    LastName = "Bur",
                    Email = "viktor.bur@gmail.com",
                    BirthDay = new DateTime(1960,11,02),
                    RegisteredAt = new DateTime(2021,06,30),
                    TeamId = 2
                },
                new
                {
                    Id = 5,
                    FirstName = "Yaroslav",
                    LastName = "Vitok",
                    Email = "yaroslav.vitok@gmail.com",
                    BirthDay = new DateTime(2001,01,14),
                    RegisteredAt = new DateTime(2021,06,30),
                    TeamId = 3
                }
            );
            modelBuilder.Entity<Project>().HasData( 
                new 
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 1,
                    Name = "BestProject",
                    Description = "Best ever made project",
                    Deadline = new DateTime(2030,10,10),
                    CreatedAt = new DateTime(2021,06,30)
                });
            modelBuilder.Entity<Task>().HasData(
                new 
                {
                    Id = 1,
                    Description  = "Clean kot",
                    Name = "Clean a Cat",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = (TaskState)3,
                    CreatedAt = new DateTime(2021,06,30),
                    FinishedAt = new DateTime(2020,10,20),
                },
                new
                {
                    Id = 2,
                    Description  = "Clean Pes",
                    Name = "Clean a Dog",
                    PerformerId = 2,
                    ProjectId = 1,
                    State = (TaskState)3,
                    CreatedAt = new DateTime(2021,06,30),
                    FinishedAt = new DateTime(2021,10,20),
                });
        }
    }
}