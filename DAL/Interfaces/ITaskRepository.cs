using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
        
    }
}