using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
        
    }
}