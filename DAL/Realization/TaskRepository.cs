using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ProjectContext _db;

        public TaskRepository(ProjectContext context)
        {
            _db = context;
        }
        
        public async System.Threading.Tasks.Task<IEnumerable<Task>> GetAll()
        {
            return await _db.Tasks
                .Include(t => t.Performer)
                .Include(t => t.Performer.Team)
                .Include(t => t.Project).ToListAsync();
        }

        public System.Threading.Tasks.Task<Task> Get(int id)
        { 
            var task = _db.Tasks
                .Include(t => t.Performer)
                .Include(t => t.Project)
                .FirstOrDefaultAsync(t => t.Id == id);
            if (task == null) throw new Exception();
            return task;
        }

        public void Create(Task item)
        {
            _db.Tasks.Add(item);
        }

        public async System.Threading.Tasks.Task Update(Task item)
        {
            var task = await _db.Tasks.FirstOrDefaultAsync(t => t.Id == item.Id);
            if (task != null)
            {
                task.Description = item.Description;
                task.Name = item.Name;
                task.Performer = item.Performer;
                task.Project = item.Project;
                task.State = item.State;
                task.FinishedAt = item.FinishedAt;
            }
        }

        public void Delete(Task item)
        {
            _db.Tasks.Remove(item);
        }
    }
}