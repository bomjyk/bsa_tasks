using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.DAL.Realization
{
    public class TeamRepository : ITeamRepository
    {
        private readonly ProjectContext _db;
        public TeamRepository(ProjectContext context)
        {
            _db = context;
        }
        
        public async Task<IEnumerable<Team>> GetAll()
        {
            return await _db.Teams.ToListAsync();
        }

        public async Task<Team> Get(int id)
        {
            return await _db.Teams.FindAsync(id);
        }

        public void Create(Team item)
        {
            _db.Teams.Add(item);
        }

        public async System.Threading.Tasks.Task Update(Team item)
        {
            var team = await _db.Teams.FirstOrDefaultAsync(t => t.Id == item.Id);
            if (team != null)
            {
                team.Name = item.Name;
            }
        }

        public void Delete(Team item)
        {
            _db.Teams.Remove(item);
        }
    }
}