using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.DAL.Realization
{
    public class UserRepository : IUserRepository
    {
        private readonly ProjectContext _db;
        public UserRepository(ProjectContext context)
        {
            _db = context;
        }
        public async Task<IEnumerable<User>> GetAll()
        {
            return await _db.Users
                .Include(u => u.Team).ToListAsync();
        }

        public async Task<User> Get(int id)
        {
            User user = await _db.Users
                .Include(u => u.Team)
                .FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                throw new Exception();
            }
            return user;
        }

        public void Create(User item)
        {
            _db.Users.Add(item);
        }

        public async System.Threading.Tasks.Task Update(User item)
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == item.Id);
            if (user != null)
            {
                user.Email = item.Email;
                user.Team = item.Team;
                user.BirthDay = item.BirthDay;
                user.FirstName = item.FirstName;
                user.LastName = item.LastName;
            }
        }

        public void Delete(User item)
        {
            _db.Users.Remove(item);
        }
    }
}