using System.Threading.Tasks;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Realization
{
    public class UnitOfWork : IUnitOfWork
    {
        public IProjectRepository Projects { get; }
        public ITaskRepository Tasks { get; }
        public ITeamRepository Teams { get; }
        public IUserRepository Users { get; }
        private readonly ProjectContext _context;

        public UnitOfWork(IProjectRepository projectRepository,
            ITaskRepository taskRepository,
            ITeamRepository teamRepository,
            IUserRepository userRepository,
            ProjectContext context)
        {
            Projects = projectRepository;
            Tasks = taskRepository;
            Teams = teamRepository;
            Users = userRepository;
            _context = context;
        }
        public async Task<int> Save()
        {
            return await _context.SaveChangesAsync();
        }
    }
}