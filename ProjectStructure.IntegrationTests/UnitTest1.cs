using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using ProjectStructure.Domain.DTO.Project;

namespace ProjectStructure.IntegrationTests
{
    public class IntegrationTests : IClassFixture<WebApplicationFactory<ProjectStructure.Startup>>
    {
        public HttpClient Client { get; }

        public IntegrationTests(WebApplicationFactory<Startup> fixture)
        {
            Client = fixture.CreateClient();
        }
        [Fact]
        public void CreateProject_ShouldCreate()
        {
            ProjectCreateDTO projectCreateDto = new ProjectCreateDTO()
            {
                Name = "Test projectStructure",
                AuthorId = 1,
                TeamId =  1,
                Deadline = DateTime.Now,
                Description = "Make unit tests and integrated tests for projectStructure"
            };
            string json = JsonConvert.SerializeObject(projectCreateDto);
            StringContent data = new StringContent(content: json,encoding: Encoding.UTF8,mediaType: "application/json");
            
            
            string projectCreateURI = "https://localhost:5001/api/Projects";
            var response = Client.PostAsJsonAsync(requestUri: projectCreateURI, data).Result;
            Assert.Equal(HttpStatusCode.OK,response.StatusCode);
            
        }
        
    }
}